.. apps.sumasu.jp documentation master file
.. title:: apps.sumasu.jp

==============
apps.sumasu.jp
==============

.. toctree::
   :titlesonly:
   :hidden:

   android_apps/Ore2weet
   android_apps/ucas_converter
   android_apps/twicca_multiline
   android_apps/twicca_door
   android_apps/replytter

About Developer
===============

.. table::

   +----------------+----------------------------------------------------------------------+
   | Developer Name | `ryo.sms <https://play.google.com/store/apps/developer?id=ryo.sms>`_ |
   +----------------+----------------------------------------------------------------------+
   | Twitter        | `@ryosms <https://twitter.com/ryosms>`_                              |
   +----------------+----------------------------------------------------------------------+
   | Blog           | `sms日記 <http://blog.livedoor.jp/ryosms/>`_                         |
   +----------------+----------------------------------------------------------------------+

.. raw:: html

   <a href="https://androider.jp/developer/ed50a020da5c984d1ccf5279104000a8/?from=developerBanner" title="アンドロイダー公認デベロッパー認証">
   <img src="https://androider.jp/devbanner/ed50a020da5c984d1ccf5279104000a8/androider_certification.png" alt="アンドロイダー公認デベロッパー認証"></a>

Android Apps
============

- :doc:`android_apps/Ore2weet`
- :doc:`android_apps/ucas_converter`
- :doc:`android_apps/twicca_multiline`
- :doc:`android_apps/twicca_door`
- ユーザーリンクプラグイン（User Link Plugin for twicca）
- u_s_k プラグイン（u_s_k Plug-in for twicca）
- :doc:`android_apps/replytter`
- ゴルフ場検索（Japanese Only）
- Location Saver（Location Saver）

|


:ref:`genindex`

