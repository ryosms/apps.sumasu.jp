.. title:: リプライったー

.. index::
    single: Android Apps;Replytter
    single: Android Apps;リプライったー

リプライったー（Replytter）
===========================

| Twitterでフォローしている／フォローされている人の一覧を取得するアプリです。
| Androidの共有機能を使うことで、様々なクライアントで使用することができます。


主な特徴
--------

- よく会話する人を保存しておける
- 複数選択して共有できる
- Simejiマッシュルームに対応

ダウンロード
------------

`Google Play <https://play.google.com/store/apps/details?id=jp.sumasu.followerpicker>`_ からダウンロード、インストールしてください。

.. image:: /images/get_it_on_google_play.png
    :target: https://play.google.com/store/apps/details?id=jp.sumasu.followerpicker
    :alt: Get it on Google Play

関連リンク
----------

- `Androidアプリ第二弾『リプライったー』を公開しました - sms日記 <http://blog.livedoor.jp/ryosms/archives/928943.html>`_
- `リプライったーを更新しました - sms日記 <http://blog.livedoor.jp/ryosms/archives/6223548.html>`_

- `リプライったー - アンドロイダー <http://androider.jp/a/74bdcb7f8ec6bf99/>`_

更新履歴
--------

.. table::

    =========  ============  ==================================================
     version    公開日        更新内容
    =========  ============  ==================================================
     1.0.0      2010/09/09   | 公開開始
     1.1.0      2010/10/01   | アイコン画像の取扱変更
                             | その他バグ修正
     1.1.1      2010/10/09   | アイコン画像の取得でAPIを大量に消費する問題修正
     1.1.2      2010/10/13   | SDインストール対応
     1.1.3      2010/10/13   | ver.1.1.2のバグ修正
     1.1.4      2010/12/06   | 画面回転時の挙動・リスト再読み込み時の挙動修正
     2.0.0      2012/05/17   | フルスクラッチで作り直し
                             | Twitter APIの改定に対応
                             | スワイプでの画面遷移に対応
     2.0.1      2012/05/22   | マッシュルームから呼び出せないバグ修正
     2.1.0      2013/03/03   | Twitter API1.1対応
    =========  ============  ==================================================
