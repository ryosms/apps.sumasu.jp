.. title:: 俺々ツイート設定方法

.. index::
    single: Twitter Application Management
    single: Consumer Key(API Key)
    single: Consumer Secret(API Secret)
    single: Callback URL


俺々ツイート設定方法
====================

0. アカウント登録
-----------------

- twitterのアカウントを持っていない場合は `Signupページ <https://twitter.com/signup>`_ でアカウントを登録します。

1. アプリケーションの登録
-------------------------

- `Twitter Application Management <https://apps.twitter.com>`_ ページにアクセスします。
- ``Create New App`` ボタンをクリックします。

    .. image:: /images/ore2weet/create_app.png
        :alt: Create New App

- ``Name`` 、 ``Description`` 、 ``Website`` を入力したら [#]_ 、``Developer Agreement`` を確認して ``Yes, I agree`` にチェックを入れて ``Create your Twitter application`` ボタンをクリックします。

    .. image:: /images/ore2weet/create_app2.png
        :alt: Create an application

    .. [#] Callback URLは必須ではありません。
        入力してもしなくても大丈夫ですが、入力した場合はクライアント情報の登録時に同じURLを入力する必要があります。

2. アプリケーションの設定変更
-----------------------------

- 登録したアプリケーションの詳細ページで ``Permissions`` タブを表示します。
- ``Read and Write`` 、もしくは ``Read, Write and Access direct messages`` を選択して ``Update Settings`` ボタンをクリックします。

    .. image:: /images/ore2weet/app_permissions.png
        :alt: Application Permission

    .. note::
        現在のアプリにはダイレクトメッセージを送信する機能はありませんが、
        ``Read, Write and Access direct messages`` のパーミッションを選択しておくことで、
        ツイート画面から「``D username``」形式のコマンドを使用してダイレクトメッセージを送信できます。

3. アプリケーション情報の確認
-----------------------------

クライアントの登録時に必要な以下の情報を控えておきます。

    * Consumer Key(API Key) [#2]_
    * Consumer Secret(Api Secret) [#2]_
    * Callback URL [#3]_

.. [#2] Keys and Access TokensタブのApplication Settingsで確認可能
.. [#3] DetailsタブのApplication Settingsで確認可能

    .. warning::
        Consumer Secret(API Secret)は他人に知られないように注意してください。


4. クライアント登録
-------------------

登録したアプリケーションを俺々ツイートのクライアント情報に登録します。

- [``メニュー``] → [``アプリ変更``] → [``新規作成``] を選択してクライアントの新規作成画面を開きます。 [#]_

- 以下の各項目を入力して ``保存する`` ボタンをタップします。

    - アプリ名 [#]_
    - ConsumerKey
    - ConsumerSecret
    - CallbackURL [#]_

.. [#] インストール直後等で1つもクライアントが登録されていない場合はアプリを起動するとクライアントの新規作成画面を表示します。
.. [#] アプリ名は俺々ツイート内で識別するためのみに使用するため、Twitterに登録したアプリ名と異なる名前でも問題ありません。ツイートのviaにはTwitterに登録しているアプリ名が表示されます。
.. [#] Twitterのアプリ登録時にCallbackURLを入力していない場合は空白のままにします。

.. note::
    ``apps.twitter.comを参照する`` のリンクをタップすることで、`Twitter Application Management <https://apps.twitter.com>`_ ページを開き、登録内容をコピーすることができます。

5. アカウント選択
-----------------

- アカウントの選択画面 → [``新規作成``] で使用するアカウントを登録します。

6. ツイート
-----------

- Enjoy your Twitter life!

