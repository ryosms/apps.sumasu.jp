.. title:: twiccaドアー

.. index::
    single: Android Apps;twicca door
    single: Android Apps;twiccaドアー

twiccaドアー（twicca door）
===========================

| twiccaのツイート表示プラグインの動作確認用アプリです。
| これを使えばツイート表示プラグインの動作確認のためだけにツイートをしなくても大丈夫！

| ※とりあえず、α版を公開します。

実装予定
--------

- 保存機能
- レイアウトの修正
- その他（要望があれば）

apkダウンロード
---------------

:download:`最新版（0.2.0a） <apks/twicca_door_0.2.0a.apk>`

過去のバージョンは :ref:`old_apk` にあります。

関連リンク
----------

- `twiccaドアー公開 - sms日記 <http://blog.livedoor.jp/ryosms/archives/6019197.html>`_
- `twiccaドアー更新 - sms日記 <http://blog.livedoor.jp/ryosms/archives/6231081.html>`_

更新履歴
--------

.. table::

    =========  ============  ================================================
     version    公開日        更新内容
    =========  ============  ================================================
     0.1.0a     2012/03/22    α版公開（野良）
     0.2.0a     2012/05/20    レイアウトを大幅に修正
    =========  ============  ================================================

.. _old_apk:

Old apk(s)
-----------

:download:`0.1.0a <apks/twicca_door_0.1.0a.apk>`


