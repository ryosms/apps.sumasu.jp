.. title:: 俺々ツイート

.. index::
    single: Android Apps;Ore2weet
    single: Android Apps;俺々ツイート

俺々ツイート（Ore2weet）
========================

| Twitterへのツイート投稿を行うだけのアプリです。
| viaに表示されるクライアント名に自分の好きな名前を付けることができます。
| 事前に `Twitter Application Management <https://apps.twitter.com/>`_ のページで自分のアプリを登録してConsumerKeyとConsumerSecretを入手しておく必要があります。

設定方法
--------

.. toctree::
    :maxdepth: 2

    setup_ore2weet

主な特徴
--------

- 複数クライアント対応
- 複数アカウント対応
- 他アプリからの文字列共有に対応
- twiccaプラグイン対応（返信／引用返信／引用ツイート）
- クライアント情報の入力支援
- アカウント認証の自動化（ログインは手動でおこなってください）

ダウンロード
------------

`Google Play <https://play.google.com/store/apps/details?id=jp.sumasu.ore2weet>`_ からダウンロード、インストールしてください。

.. image:: /images/get_it_on_google_play.png
    :target: https://play.google.com/store/apps/details?id=jp.sumasu.ore2weet
    :alt: Get it on Google Play


関連リンク
----------

- `俺々ツイート公開しました - sms日記 <http://blog.livedoor.jp/ryosms/archives/5970693.html>`_
- `俺々ツイートアップデート - sms日記 <http://blog.livedoor.jp/ryosms/archives/6032508.html>`_
- `「俺々ツイート」も更新 - sms日記 <http://blog.livedoor.jp/ryosms/archives/6224242.html>`_

- `俺々ツイート - アンドロイダー <https://androider.jp/official/app/4e094638b91a6aa2/>`_

- `俺々ツイート - サビ抜きのサーモンとトロとマグロ 紺野あさ美ファンブログ <http://konkon7.com/blog-entry-400.html>`_


更新履歴
--------

.. table::

    =========  ============  ================================================
     version    公開日        更新内容
    =========  ============  ================================================
     0.1.0      2012/03/09   | 公開開始
     0.2.0      2012/03/25   | ツイートボタンの位置を変更するオプションを追加
                             | twicca診断メーカープラグイン対応
                             | その他バグフィックス
     0.3.0      2012/05/17   | pic.twitter.comへのメディアアップロード対応
                             | 誤爆防止用のダイアログ表示を追加
                             | その他バグフィックス
     0.4.0      2013/02/28   | Twitter API 1.1対応
                             | 文字数のカウント処理をより正確に
                             | その他バグフィックス
     0.4.1      2013/03/05   | Android 2.2以下でクラッシュする不具合を修正
     0.4.2      2014/01/17   | SSL対応
     0.5.0      2015/04/30   | いろいろ変更
    =========  ============  ================================================

