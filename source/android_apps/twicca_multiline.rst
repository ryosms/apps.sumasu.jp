.. title:: twicca改行プラグイン

.. index::
    single: Android Apps;twicca MultiLine Plug-in
    single: Android Apps;twicca改行プラグイン


twicca改行プラグイン（twicca MultiLine Plug-in）
================================================

| ツイート中に含まれる改行を有効にして表示するためのツイート表示プラグインです。
| 当時、twiccaが改行に対応してなかったから作りました。
| ※現在のtwiccaには、設定から改行を有効にすることができます。


ダウンロード
------------

`Google Play <https://play.google.com/store/apps/details?id=jp.sumasu.multiline>`_ からダウンロード、インストールしてください。

.. image:: /images/get_it_on_google_play.png
    :target: https://play.google.com/store/apps/details?id=jp.sumasu.multiline
    :alt: Get it on Google Play

関連リンク
----------

- `俺得アプリ、公開しました - sms日記 <http://blog.livedoor.jp/ryosms/archives/3552736.html>`_

更新履歴
--------

.. table::

    =========  ============  ======================
     version    公開日        更新内容
    =========  ============  ======================
     1.0        2011/06/15    公開開始
     1.1        2011/06/15    ツイート解析機能調整
     1.2        2011/06/18    ツイート解析機能調整
     1.3        2012/01/04    バグフィックス
    =========  ============  ======================

